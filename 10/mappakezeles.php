<?php
//erőforrások
$tippek_path='../9/tippek';
$dir = opendir($tippek_path);
$limit = 90;
$huzasok_szama = 5;
while ($dir && ($file = readdir($dir)) !== false) {
    if ($file != "." && $file != "..") {
        echo "$file\n";
    }
}
closedir($dir);
$tippekJson = json_decode('[
  {
    "nev": "kcs2",
    "email": "hgy2@iworkshop.hu",
    "tippek": {
      "1": 10,
      "2": 22,
      "3": 23,
      "4": 50,
      "5": 55
    }
  },
  {
    "nev": "kcs2",
    "email": "hgy2@iworkshop.hu",
    "tippek": {
      "1": 13,
      "2": 25,
      "3": 43,
      "4": 52,
      "5": 58
    }
  }
]',true);
$sorsolas = generateSzelveny();
$output = '<h1>A sorsolás eredménye: '.implode(',',$sorsolas).'</h1>';

//var_dump('<pre>',$sorsolas);
$output .= '<ul>';
$i=1;//sorszám
//json bejárása és találatok számítása
foreach($tippekJson as $szelveny){
    $talalatok = array_intersect($szelveny['tippek'],$sorsolas);
    $talalatok_szama = count($talalatok);
    //var_dump($talalatok,$talalatok_szama);
    $output.='<li><b>'.$i++.'</b> | nev:'.$szelveny['nev'].',email:'.$szelveny['email'].', tippek:'.implode(';',$szelveny['tippek']).' | találatok száma: '.$talalatok_szama.' | találatok: '.implode(',',$talalatok).'</li>';
}
$output .= '</ul>';
echo $output;//kiírás egy lépésben
/**
 * Szelvény generálása
 * @see valami
 * @version 1.0
 * @param int $huzasok_szama
 * @param int $limit
 * @return array|bool
 * @todo ellenőrizni 6/45, 7/35, 20/80 (kenó) állapotokra
 */
function generateSzelveny($huzasok_szama = 5, $limit = 90){
    //global $huzasok_szama,$limit;//az eljárás látja ezeket a változókat
    $szelveny = [];//itt lesznek a tippek

    if($limit < $huzasok_szama){//ha hülyén paramétereztek(végtelen ciklus) lépjünk ki!
        trigger_error('Hiba a paraméterezésben! limit legyen nagyobb mint a húzások száma!',E_USER_ERROR);
        return false;
    }
    while(count($szelveny) < $huzasok_szama){
        array_push($szelveny, rand(1,$limit) );//hozzáad a tömbhöz egy elemet olyan, mint a $szelveny[] = ...
        $szelveny = array_unique($szelveny);//ismétlődő számok kiiktatása
    }
    sort($szelveny);//rendezés
    return $szelveny;
}