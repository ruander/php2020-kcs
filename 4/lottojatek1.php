<?php
//lottójáték 5/90
//5 szelvény
//sorsolás
//találat számítás

//erőforrások
$limit = 90;
$huzasok_szama = 5;

$szelveny = generateSzelveny(7,35);
echo '<pre>'.var_export($szelveny,true).'</pre>';

$szelveny = generateSzelveny(6,45);
echo '<pre>'.var_export($szelveny,true).'</pre>';

$szelveny = generateSzelveny();
echo '<pre>'.var_export($szelveny,true).'</pre>';

//hibás futtatás teszt
$szelveny = generateSzelveny(100);
echo '<pre>'.var_export($szelveny,true).'</pre>';



echo nevAtalakito('Horváth György János');

/**
 * function eljarasNeve(parameter1, parameter2 = 'alapérték'){
    eljárás működése

    return $valami //visszatérési érték
 }
 */

/**
 * Szelvény generálása
 * @see valami
 * @version 1.0
 * @param int $huzasok_szama
 * @param int $limit
 * @return array|bool
 * @todo ellenőrizni 6/45, 7/35, 20/80 (kenó) állapotokra
 */
function generateSzelveny($huzasok_szama = 5, $limit = 90){
    //global $huzasok_szama,$limit;//az eljárás látja ezeket a változókat
    $szelveny = [];//itt lesznek a tippek

    if($limit < $huzasok_szama){//ha hülyén paramétereztek(végtelen ciklus) lépjünk ki!
        trigger_error('Hiba a paraméterezésben! limit legyen nagyobb mint a húzások száma!',E_USER_ERROR);
        return false;
    }
    while(count($szelveny) < $huzasok_szama){
        array_push($szelveny, rand(1,$limit) );//hozzáad a tömbhöz egy elemet olyan, mint a $szelveny[] = ...
        $szelveny = array_unique($szelveny);//ismétlődő számok kiiktatása
    }
    sort($szelveny);//rendezés
    return $szelveny;
}

/**
 * Kiemeli az első tagját a névnek
 * @param $name | pl: 'John Doe'
 * @return string '<b>John</b> Doe
 */
function nevAtalakito($name){

    //szöveg szétválasztása a spaceknél -> tömb
    $nevtomb = explode(' ',$name);// a tipus itt tömb, az egyes elemei a szétválasztott elemek
    $atalakitott_nev = '<b>'.$nevtomb[0].'</b> ';//első elem a tömbben kap b TAGeket
//    echo '<pre>'.var_export($nevtomb,true).'</pre>';
    unset($nevtomb[0]);//kivesszük a tömbből a már létező vastagbetűs elemet
//    echo '<pre>'.var_export($nevtomb,true).'</pre>';
//    echo '<pre>'.var_export($atalakitott_nev,true).'</pre>';
    $atalakitott_nev .= implode(' ',$nevtomb);
    return $atalakitott_nev;
}