<?php
//lottójáték 5/90
//5 szelvény
//sorsolás
//találat számítás
$limit = 35;//1-limit közötti értékek
$huzasok_szama = 7;//ennyi db tipp egy szelvényen
$szelvenyek_szama = 5;//ennyi szelvényt szeretnénk

$szelvenyek  =[];//itt lesznek a szelvények

for($i=1;$i<=$szelvenyek_szama;$i++){
    array_push($szelvenyek, generateSzelveny($huzasok_szama,$limit));
}
//echo '<pre>'.var_export($szelvenyek,true).'</pre>';

//sorsolás
$sorsolas = generateSzelveny($huzasok_szama,$limit);
//echo '<pre>'.var_export($sorsolas,true).'</pre>';

$output = "<h1>Lottójáték $huzasok_szama/$limit</h1>";
$nyeroszamok = implode(',',$sorsolas);//sorsolás tömb értékei vesszővel elválsztva (string)
$output .= "<h2>Nyerőszámok: $nyeroszamok</h2>";
$output .= "<ul>";
//járjuk be a szelvények halmazát és egy egy szelvény és a sorsolás metszése kiadja a közös elemeket (találatok száma)
foreach($szelvenyek as $k => $szelveny){
    $talalatok_halmaz = array_intersect($szelveny,$sorsolas);
    $talalatok_szama = count($talalatok_halmaz);
    //echo '<pre>'.var_export($talalatok_halmaz,true).'</pre>';
    // 0: tippek: 1,34,56,78,79 | 2 találat: 34, 78 :)
    //vagy
    // 0: tippek: 1,34,56,78,79 | nincs találat! :(
    $output .= "<li>$k: tippek: ".implode(',',$szelveny). " | ";
    if($talalatok_szama > 0 ){//ha van találat
        $output .= "$talalatok_szama találat: ".implode(',',$talalatok_halmaz)." :)";
    }else{//nincs találat
        $output .= 'Nincs találat! :(';
    }
    $output .= "</li>";//listaelem zárása
}//end foreach
$output .= "</ul>";
echo $output;//kiírás egy lépésben


/**
 * Szelvény generálása
 * @see valami
 * @version 1.0
 * @param int $huzasok_szama
 * @param int $limit
 * @return array|bool
 * @todo ellenőrizni 6/45, 7/35, 20/80 (kenó) állapotokra
 */
function generateSzelveny($huzasok_szama = 5, $limit = 90){
    //global $huzasok_szama,$limit;//az eljárás látja ezeket a változókat
    $szelveny = [];//itt lesznek a tippek

    if($limit < $huzasok_szama){//ha hülyén paramétereztek(végtelen ciklus) lépjünk ki!
        trigger_error('Hiba a paraméterezésben! limit legyen nagyobb mint a húzások száma!',E_USER_ERROR);
        return false;
    }
    while(count($szelveny) < $huzasok_szama){
        array_push($szelveny, rand(1,$limit) );//hozzáad a tömbhöz egy elemet olyan, mint a $szelveny[] = ...
        $szelveny = array_unique($szelveny);//ismétlődő számok kiiktatása
    }
    sort($szelveny);//rendezés
    return $szelveny;
}