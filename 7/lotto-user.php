<?php
//külső file betöltése, mintha ide lenne gépelve
//include "functionns.php";//ha nincs meg a file, warning és halad a köv sorra
require "functions.php";//ha nincs meg a file, die
include_once "functions.php";//ha be volt már töltve fatal error mert function declarációk vannak
require_once "functions.php";// _once, ha már volt includeja a file nak nem tölti újra

$huzasok_szama = 5;//ennyi szám kell
$limit = 90;//1- limitig van a számok alső és felső határa

if (!empty($_POST)) {
    $hiba = [];
    //név min 3 karakter
    $nev = filter_input(INPUT_POST, 'name');
    if (mb_strlen($nev, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Név túl rövid!</span>';
    }
    //email kötelező és email formátum
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem megfelelő formátum!</span>';
    }
    //tipp 1 és 90 közé kell essen
    //filter közben
    //hc filter minta: example 1: https://www.php.net/manual/en/function.filter-input-array.php
    /*$options = [
      'options' => [
              'min_range' => 1,
              'max_range' => $limit,
      ]
    ];
    $tipp_1 = filter_input(INPUT_POST,'tipp-1',FILTER_VALIDATE_INT,$options);

    if(!$tipp_1){
        $hiba['tipp-1']='posijoij';
    }*/
    //args on keresztül állítjuk be a szűrést a tippekre
    $args = [
        'tippek' => [
            'filter' => FILTER_VALIDATE_INT,
            'flags' => FILTER_REQUIRE_ARRAY,
            'options' => [
                'min_range' => 1,
                'max_range' => $limit
            ]
        ]
    ];
    $tippek_szures = filter_input_array(INPUT_POST, $args);
    //var_dump('<pre>', $tippek_szures);//hibakereséshez
    //szűrt tömböt bejárva kialakíthatjuk a hibatömb elemeit
    foreach($tippek_szures['tippek'] as $k => $v){

        //echo "<br>$v";//hibakereséshez
        if(false === $v){//ha az adott kulcson falset találunk akkor az a szűrésen elbukott, így a hiba tömb hasonló szerkezetre megkapja a hiba üzenetet
            //die('falset találtam');//hibakereséshez tettük be
            $hiba['tippek'][$k]='<span class="error">Nem megfelelő formátum!</span>';
        }
    }
    //var_dump('<pre>', $hiba);

    if (empty($hiba)) {
        //nincs hiba
        //die('ok');
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottójáték</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
        }

        fieldset {
            margin: 0;
            padding: 0.5em;
            border: 0;
            background: #efefef;
            display: flex;
            flex-flow: column nowrap;
        }

        legend {
            background: #efefef;
            padding: 0.5em;
        }

        .error {
            display: block;
            color: red;
            font-style: oblique;
            font-size: .7em;
            line-height: 1.5em;
        }

        input {
            display: block;
        }
    </style>
</head>
<body>
<h1>Lottójáték</h1>
<form method="post" id="szelveny">
    <fieldset>
        <legend>Személyes adatok</legend>
        <label>
            Név<sup>*</sup>:
            <input type="text" name="name" id="name" value="<?php echo checkValue('name') ?>">
            <?php echo hibaKiir('name'); ?>
        </label>
        <label>
            Email<sup>*</sup>:
            <input type="text" name="email" id="email" value="<?php echo checkValue('email'); ?>"
                   placeholder="email@cim.hu">
            <?php echo hibaKiir('email'); ?>
        </label>
    </fieldset>
    <fieldset>
        <legend>tippsor</legend>
        <?php
        //ciklus a tippmezőknek
        for ($i = 1; $i <= $huzasok_szama; $i++) {
            echo '<label>
            Tipp ' . $i . '<sup>*</sup>:
            <input type="text" name="tippek[' . $i . ']" id="tipp-' . $i . '" value="' . checkValue('tippek['.$i.']') . '">';//label nyitás input kiírás
            echo hibaKiir2('tippek',$i);//hibakiírás
            echo '</label>';
        }
        ?>
        <!--<label>
            Tipp 1<sup>*</sup>:
            <input type="text" name="tippek[1]" id="tipp-1" value="<?php /*echo checkValue('tipp-1') */ ?>">
            <?php /*echo hibaKiir('tipp-1'); */ ?>
        </label>-->

    </fieldset>
    <button>Tippsor beküldése</button>
</form>
</body>
</html>