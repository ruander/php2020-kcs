<?php
//switch bemutatása
/**
 * switch(var){
 case 'other value':
                ....
            break;

 case 'value':
                ....
            break;
 default:
                ...
            break;

 }
 */
$muvelet = 'update';

switch($muvelet){
    case 'delete':
        echo 'törlés eset: id(x)';
        break;
    case 'update':
        echo 'Módosítás eset: id(x) - adatlekérés';
        //break;
    case 'create':
        echo 'Új létrehozás eset: form';//ha van adat betöltjük az űrlapba
        break;
    default:
        echo 'alap eset van: CRUDnál listázunk';
        break;//kilép a switchből a következő sorba ami utána van
}