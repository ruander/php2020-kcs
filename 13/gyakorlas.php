<?php
require "connect.php";//db csatlakozás

//echo '<pre>'.var_export($link,true).'</pre>';//objektum, ez kell az adatbézis eljárások paraméterezéséhez
$qry = "SELECT employeenumber,
            CONCAT(firstname,' ',lastname) fullname, 
            extension,
            jobtitle
            FROM employees
            ORDER BY fullname ";
$result = mysqli_query($link,$qry) or die('hiba van:'. mysqli_error($link));

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP és MySQL a gyakorlatban</title>
</head>
<body>
<table border="1">
    <tr>
        <th>Azonosító</th>
        <th>Név</th>
        <th>Mellék</th>
        <th>Beosztás</th>
    </tr>

<?php
while(null !== $row = mysqli_fetch_assoc($result)){
    //echo '<pre>'.var_export($row,true).'</pre>';
    echo '<tr>
            <td>'.$row['employeenumber'].'</td>
            <td>'.$row['fullname'].'</td>
            <td>'.$row['extension'].'</td>
            <td>'.$row['jobtitle'].'</td>
          </tr>';
}
//echo mysqli_num_rows($result);

?>
</table>
</body>
</html>