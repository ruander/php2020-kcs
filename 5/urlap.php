<?php
//űrlap adatok feldolgozása
/*
echo '<pre>';
var_dump($_GET);//szuperglobális GET tömb
var_dump($_POST);//szuperglobális POST tömb
var_dump($_REQUEST);//szuperglobális REQUEST tömb
echo '</pre>';
*/
if(!empty($_POST)){//ha nem üres a POST azaz küldtek POST tipusu adatot
    $hiba = [];//üres hibatömb, ide gyűjtjük a mező hibákat
    /*
    var_dump($_POST['adat']);//így ne!!!
    //adatmező adatkinyerése sose legyen közvetlen kulcsról sem POSTról SEM GETről
    */
    $adat = filter_input(INPUT_POST, 'adat'); //https://www.php.net/manual/en/function.filter-input.php
    //die($adat);//megáll a kódfutás
    //ha az adatnak egész számnak kell lennie alkalmazzunk filtert | https://www.php.net/manual/en/filter.filters.validate.php
    $szam = filter_input(INPUT_POST, 'adat',FILTER_VALIDATE_INT);
    /*echo '<pre>';
    var_dump($adat, $szam);
    die();*/
    if($szam<1){
        //hiba van
        $hiba['adat'] = '<span class="error">Hibás adat! A beírt érték nem 0 nál nagyobb egész szám</span>';
    }/*else{
        //nincs hiba
        die('Az adat jó!');
    }*/
    if(empty($hiba)){
        //üres maradt a hibatömb, mehetnek a műveletek
        die('Az adatok jók!');
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlap - adatfeldolgozás</title>
</head>
<body>
<!--form>input:text+button{küldés}-->
<form method="post">
    <label>Pozitív egész szám<sup>*</sup>:
        <input type="text" name="adat" value="<?php echo filter_input(INPUT_POST,'adat'); ?>" placeholder="24">
        <?php
        //hibaüzenet kiírása, ha létezik a mezőnek hibája
        if(isset($hiba['adat'])) echo $hiba['adat'];//ha nincs {} az if után (SE ; !!!!) akkor a következő utasítást hajtja végre (vagy nem)
        ?>
    </label>
    <!--hidden mező az űrlapon nem látható de a forráskódban igen és az adat szerepelni fog az űrlap adatok között
    <input type="hidden" name="a" value="85">-->
    <br><button>küldés</button>
</form>
</body>
</html>

