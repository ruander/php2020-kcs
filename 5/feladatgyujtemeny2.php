<?php
//űrlap adatok feldolgozása
if(!empty($_POST)){//ha nem üres a POST azaz küldtek POST tipusu adatot
    $hiba = [];//üres hibatömb, ide gyűjtjük a mező hibákat

    //0 nál nagyob egész szám érkezett e?
    $szam = filter_input(INPUT_POST, 'adat',FILTER_VALIDATE_INT);
    if($szam<1){
        //hiba van
        $hiba['adat'] = '<span class="error">Hibás adat! A beírt érték nem 0 nál nagyobb egész szám</span>';
    }

    if(empty($hiba)){
        //üres maradt a hibatömb, mehetnek a műveletek
        //@todo: 8,9,10,12,13,14,15,16
        $string = 'XO';
        echo '<div>';
        echo '<b>8. Készítsünk programot, amely bekér egy N természetes számot, majd kirajzol a képernyőre egymás mellé N-szer az "XO" betűket és a kiírás után a kurzort a következő sor elejére teszi. </b>';
        echo '<br>';
        //$szamnyi string
        for($i=1;$i<=$szam;$i++){
            echo $string;
        }
        echo '</div>';

        //
        echo '<b>20. Kérjünk be egy természetes számot (a), majd rajzoljunk ki a képernyőre egy háromszöget csillagokból (*). A háromszög a sornyi csillagból álljon. </b><br>';
        $a = $szam;//a feladatban 'a' változó szepel
     /* a = 4
          *
         ***
        *****
       *******
     */
        for($i=1;$i<=$a;$i++){
            //echo str_repeat('&nbsp;',$a-$i);//spacek
            for($j=1;$j<=$a-$i;$j++){//ciklus a sor eleji spacek-nek
                echo '&nbsp';
            }
            for($j=1;$j<=($i*2)-1;$j++){//ciklus a csillagoknak
                echo '*';
            }
            echo '<br>';
        }

        die('Feladatmegoldások vége');
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Űrlap - adatfeldolgozás</title>
</head>
<body>
<!--form>input:text+button{küldés}-->
<form method="post">
    <label>Pozitív egész szám<sup>*</sup>:
        <input type="text" name="adat" value="<?php echo filter_input(INPUT_POST,'adat'); ?>" placeholder="24">
        <?php
        //hibaüzenet kiírása, ha létezik a mezőnek hibája
        if(isset($hiba['adat'])) echo $hiba['adat'];//ha nincs {} az if után (SE ; !!!!) akkor a következő utasítást hajtja végre (vagy nem)
        ?>
    </label>
    <!--hidden mező az űrlapon nem látható de a forráskódban igen és az adat szerepelni fog az űrlap adatok között
    <input type="hidden" name="a" value="85">-->
    <br><button>küldés</button>
</form>
</body>
</html>

