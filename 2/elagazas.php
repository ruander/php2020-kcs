<?php
//elágazó programrészek
//írjuk ki a számokat 1től 25ig, a párosokat kékkel a páratlanokat pirossal
/*
if(feltétel){
    igaz ág
}else{
    hamis ág
}
 */
for($i=1;$i<=25;$i++){
    //elágazásban beállítunk egy szin változót, minden ciklusmag végrehajtáskor lefut és beállítja az aktuális értéket
    if($i%2 == 0){//operátor: % -> maradékos osztás; == -> értékegyenlőség vizsgálat
        //0 a maradék tehát páros
        $color = '#04c';
    }else{
        //maradék nem 0 tehát páratlan
        $color = '#c40';
    }
    echo '<div style="color: '.$color.'">'.$i.'</div>';//belefűzzük a megfelelő helyre az összeállított színt, ami a $color változóba került
}
