<?php
//kezdés: 'dobjunk 1 kockával' és mondjuk meg az eredményt
$dobas = rand(1,6);
echo "A dobás értéke: $dobas";
//dobjunk megint és mondjuk meg az összegüket
$dobas2 = rand(1,6);
echo "<br>A 2. dobás értéke: $dobas2,  a két dobás értéke összesen $dobas + $dobas2";//ez így nem az igazi
$osszeg = $dobas + $dobas2;
echo "<br>A 2. dobás értéke: $dobas2,  a két dobás értéke összesen $osszeg.";//megoldas 1 segédváltozó
echo "<br>A 2. dobás értéke: $dobas2,  a két dobás értéke összesen ".($dobas + $dobas2).".";//megoldas 2 fűzés
//Dobjunk 12 szer és írjuk ki a dobásokat és az összegüket, többször ismétlődő programrész, ciklus

/*
 for(ciklusváltozó kezdeti értéke; belépési feltétel vizsgálat; ciklusváltozó léptetése){
    ciklusmag
}
 */
//összeg számlálás
$sum = 0;//összeg kezdeti értéke legyen 0
for($i=1; $i<=12 ;  $i++ ){// $i = $i + 1 -> $i++; $i = $i - 1 -> $i--
    $dobas = rand(1,6);//tároljuk el a dobást egy primitivként

    $sum = $sum + $dobas;//adjuk az összeghez
    echo '<br>'.$dobas;
}
//kiírjuk az összegyűjtött összeget
echo '<br><b>Összérték:</b> '.$sum;
//így a ciklus után az egyes dobások nem érhetőek el , csak az utolsó a $dobas -ban, ha szükség van az egyes értékekre később, gyűjtsük halmazba
$dobasok = [];//üres tömb az egyes dobásoknak
for($i=1;$i<=12;$i++){
    $dobasok[] = rand(1,6);
}
//dobások
echo '<pre>'.var_export($dobasok,true).'</pre>';
echo '<br><b>Összérték:</b> '.array_sum($dobasok);//tömb értékeinek összege

//12.Írjon egy php programot, amely előállít egy XxY cellát tartalmazó táblázatot és mindegyik cellában a Google szót helyezi el a cellák jobb felső sarkába.
//most egy 4 soros 5 oszlopos táblázatot készítünk
//elemi táblázatból indulunk
$tabla = '<table border="1">';//table TAG nyitás
for($sor = 1; $sor <= 4 ;$sor++) {
    $tabla .= '<tr>';//sor nyitás
    for ($oszlop = 1; $oszlop <= 5; $oszlop++) {//beágyazott ciklus a celláknak
        $tabla .= "<td>google ($sor / $oszlop)</td>";//cella(k) - ciklusváltozók a ciklusmagban elérhetőek
    }
    $tabla .= '</tr>';//sor zárás
}
$tabla .= '</table>';//table TAG nyitás

echo $tabla;
// @todo: haz-feladatok - teljes; feladatgyüjtemény 1 - 8 ig