<?php
//nem primitív változók
$tomb = array();//üres tömb létrehozása
//változó kiírása fejlesztés közben 2, ez string fog visszatérni tehát ki kell írni
echo '<pre>'.var_export($tomb, true).'</pre>';

$tomb = [];//üres tömbbel felülírjuk az amúgy is üres tömböt, újradeklarálás
$tomb = ['alma', 55, true, 7/6, pi()];//tömb létrehozása/újradeklarálása elemekkel | egydimenziós tömb | kulcs => érték párosokkal
echo '<pre>'.var_export($tomb, true).'</pre>';

$tomb[] = 'Hello World';//tömb bővítése automatikus kulcsmegadással
echo '<pre>'.var_export($tomb, true).'</pre>';

echo $tomb[3];//tömb egy megadott elemének kiírása

$tomb[100] = 55/3;//irányított kulcsra elem elhelyezése
echo '<pre>'.var_export($tomb, true).'</pre>';
$tomb[] = 'teszt';
echo '<pre>'.var_export($tomb, true).'</pre>';

$tomb['username'] = "Horváth György";//asszociatív kulcsra illesztés
echo '<pre>'.var_export($tomb, true).'</pre>';
//asszociatív kulcson tárolt elem elérése:
echo 'Felhasználó neve: '.$tomb['username'];
//felhasználására egy példa, több asszociatív kulcs alkalmazása
$user = [
    'name' => 'Horváth György',//kulcs => érték
    'email' => 'hgy@iworkshop.hu',
    'password' => 'áporjeovgihrepg', //elkódolatlanul soha nem tárolunk jelszót!
];
//felhasználó adatainak kiírása egy listába pl.
$lista = '<ul>';//lista nyitása a stringbe
$lista .= '<li>elem</li>';//ugyanaz mint $lista = $lista . '<li>elem</li>', ugyanígy működik a +=, -= , *=
$lista .= '<li><b>Név:</b> '.$user['name'].'</li>';//név elem listába fűzése
$lista .= '<li><b>Email:</b> '.$user['email'].'</li>';
$lista .= '</ul>';//lista TAG zárása
//kiírás 1 lépésben a teljes lista változóra, ami így tartalmazza a 3 elemet is
echo $lista;

//tömb műveletek
echo $elemszam = count($tomb);//elemszám eltárolása változóba és kiírása egy lépésben
/* csak bemutatás céljára, ha egy tömb eljárás nem tömböt kap, hibát eredményez
$szoveg = 'ez egy szöveg';
echo '<br>';//új sor
echo $elemszam_szoveg = count($szoveg);//hiba!!! a count csak TÖMB típust tud kezelni
*/
