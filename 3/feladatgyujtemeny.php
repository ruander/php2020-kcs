<?php
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.
$menupontok = [
    1 => 'Home',
    2 => 'About',
    3 => 'Services',
    4 => 'Contact',
    5 => 'Extra'
];
$menu = '<nav><ul>';
$elemszam = count($menupontok);
for($i=1;$i<=$elemszam;$i++){
    //egy listaelem 1 menüpont, és a menüpontokat tartalmazó tömbnek a szerkezete kötött!!!!!!
    $menu .= '<li><a href="#'.$menupontok[$i].'">'.$menupontok[$i].'</a></li>';
    //echo $menupontok[$i];
}
$menu .='</ul></nav>';
echo $menu;



//2. Készítsünk programot, amely kiszámolja az első 100 darab. természetes szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)
$sum=0;
for($i=1;$i<=100;$i++){
    $sum+=$i;
}
echo '<h2>A számok összege 1-100ig: '.$sum.'</h2>';

// 3. Készítsünk programot, amely kiszámolja az első 7 darab. természetes szám szorzatát egy ciklus segítségével. (A szorzat kiszámolásához vezessünk be egy változót, amelyet a program elején beállítunk 1-re, a ciklusmagban pedig mindig hozzászorozzuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, ..., 7 számokat.)
$mtpl=1;
for($i=1;$i<=7;$i++){
    $mtpl*=$i;
}
echo '<h2>A számok szorzata 1-7ig: '.$mtpl.'</h2>';

//4. Készítsünk programot, amely kiszámolja az első 100 darab. páros szám összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban vegyük a ciklusváltozó kétszeresét - így megkapjuk a páros számokat. Ezeket hasonlóan adjuk össze, mint az első feladatban).

$sum=0;
for($i=2;$i<=100;$i+=2){
    $sum+=$i;
}
echo '<h2>A páros számok összege 1-100ig: '.$sum.'</h2>';

//5. Készítsünk programot, amely kiszámolja az első 100 darab. páratlan szám összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban vegyük a ciklusváltozó kétszeresét eggyel csökkentve - így megkapjuk a páratlan számokat. Ezeket hasonlóan adjuk össze, mint az első feladatban).
$sum=0;
for($i=1;$i<=100;$i+=2){
    $sum+=$i;
}
echo '<h2>A páratlan számok összege 1-100ig: '.$sum.'</h2>';

//házi feladatok txt 19.
$elem = "";
for($i=1;$i<=4;$i++){
    echo '<br>'.$elem.="2";
}
echo '<hr>';
//beágyazott ciklus

for($sor=1;$sor<=4;$sor++){//ciklus a soroknak

    for($i=1;$i<=$sor;$i++){//belső ciklus a kiírandó elemeknek, mindig annyi elemet írunk ki ahányadik sorban vagyunk
        echo "2";
    }

    echo '<br>';
}
//20
for($sor=4;$sor>=1;$sor--){//ciklus a soroknak, most visszafele fut 4,3,2,1

    for($i=1;$i<=$sor;$i++){//belső ciklus a kiírandó elemeknek, mindig annyi elemet írunk ki ahányadik sorban vagyunk
        echo "2";
    }

    echo '<br>';
}
echo '<hr>';
//ha ismered a nyelvi lehetőségeket
$letter = "8";
for($i=1;$i<=4;$i++){
    echo str_repeat($letter,$i).'<br>';
}
for($i=4;$i>=1;$i--){
    echo str_repeat($letter,$i).'<br>';
}

