<?php
$menupontok = [
    'Home',
    'About',
    'Services',
    'Contact',
    'Extra',
    'usermenu' => 'Users'
];//tömb alap értékekkel
var_dump($menupontok);
//bejárás for ciklussal - nem jó megoldás, mert lehet asszociatív is a tömb, arra ez a megoldás nem jó, és a PHP megengedi a foghíjas tömböket is ez esetben sem jó
$elemszam = count($menupontok);//5
for($i=0;$i<$elemszam;$i++){
    //az $i -edik kulcson találhatóak az elemek
    echo '<br>'.$menupontok[$i];
}

//bejárás helyesen
/*foreach(tombNeve as kulcs => ertek){
    ciklusmag (kulcs és érték párosok elérhetőek)
}
//foreach alkalmas objektum bejárásra is
*/
foreach ($menupontok as $k => $v){
    echo "<br>Aktuális kulcs: $k | érték: $v";
}
//6 különböző szám 1-20 között
$tarolo = [];
for(;count($tarolo)<6;){
    $szam = rand(1,20);
    if(false === array_search($szam,$tarolo)) {// operátor : ! -> ellenkezőjére változtatja logikailag az utána következő elemet
        //visszatérése a searchnek lehet false, vagy a találat kulcsa azaz lehe 0 is, aminek a !0 negáltja TRUE - ez hibát okoz!!!! javítása false ===
        $tarolo[] = $szam;
    }
}
echo '<pre>'.var_export($tarolo,true).'</pre>';
/*
 * ciklusváltozo = 1
 while(feltétel ellenőrzése){
    ciklusmag
ciklusvaltozo++
}
 */
$tarolo = [];//tároló alaphelyzetbe állítása
//addig futtassunk egy ciklust AMÍG az elemszáma eléri a kívánt értéket
while(count($tarolo)<6){

    //tároljuk el a véletlenszámot
    $tarolo[] = rand(1,20);
    //irtsuk ki az ismétlődéseket
    $tarolo = array_unique($tarolo,SORT_REGULAR);//az addigi elemeket ujrarendezi és engedi a utolsó ures indexet utána (pointer), a SORT_REGULAR kapcsoló ezen változtat
}
echo '<pre>'.var_export($tarolo,true).'</pre>';

//while ciklussal for()-hoz hasonló működés
/*
$i=1;
while($i<=6){
    echo '<br>'.$i;
    $i++;
}
//ua
for($i=1;$i<=6;$i++){
    echo '<br>'.$i;
}
*/
//rendezés emelkedő sorrendbe (tömb értékek)
sort($tarolo);//ujra kulcsolja 0 tól a tömböt
echo '<pre>'.var_export($tarolo,true).'</pre>';
