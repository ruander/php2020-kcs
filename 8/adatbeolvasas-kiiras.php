<?php
//users mappát létezőnek véljük
$fileName = "teszt/users/user1.json";//json file: https://hu.wikipedia.org/wiki/JSON
if(file_exists($fileName)){//ha létezik a mappa/file
    $userContent = file_get_contents($fileName);
    $userData = json_decode($userContent,true);
    var_dump("<<pre>",$userContent,$userData);
}else{
    echo "Nincs ilyen userfile: $fileName";
}
//csináljunk egy 2es teszt usert
$fileName = "teszt/users/user2.json";
$user = [
    "id" => 2,
    "name" => "Gipsz Jakab",
    "email" => "teszt@teszt.teszt"
];

$newUserContent = json_encode($user) ;//json stringgé alakítjuk a tömbböt hogy file ba lehessen írni
var_dump($user, $newUserContent);
//hozzuk létre/írjuk felül a filet
file_put_contents($fileName,$newUserContent);

//másik formátum egyszerűbb adatfeldolgozásra (excel tud ilyet exportálni)
$csvUserFile = "teszt/user.csv";
//első lépésben írjuk ki a usert (gipsz jakab) most csv formátumban
$handler = fopen($csvUserFile,"w");
fputcsv($handler,$user);
fclose($handler);
//olvassuk be a tartalmát feldolgozásra
$handler = fopen($fileName,"r");//olvasásra
$userFromCsvFile = fgetcsv($handler);//egyből átalakítva kapjuk
var_dump($userFromCsvFile);//csv nem asszociatív