<?php
require "functions.php";//7 óra anyaga

if (!empty($_POST)) {
    $hiba = [];
    //név min 3 karakter
    $nev = filter_input(INPUT_POST, 'name');
    if (mb_strlen($nev, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Név túl rövid!</span>';
    }
    //email kötelező és email formátum
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem megfelelő formátum!</span>';
    }
    //kor, kötelező, legyen 6 évnél idősebb
    $options = [
        'options' => ['min_range' => 6]
    ];
    $age = filter_input(INPUT_POST, 'age', FILTER_VALIDATE_INT, $options);
    if (!$age) {
        $hiba['age'] = '<span class="error">Nem megfelelő életkor (min 6 év) vagy formátum!</span>';
    }

    if (empty($hiba)) {
       $user = [
               'name' => $nev,
           'email'=> $email,
           'kor' => $age
       ];
       $dir = "users/";
       if(!is_dir($dir)){//mappa ellenőrzése
           mkdir($dir,0755);
       }
       /************json************/
       $fileName = "user.json";
       $fileContent = json_encode($user,true);
       file_put_contents($dir.$fileName,$fileContent);
       /************CSV***********/
       $csvFileName ="user.csv";
       $handler = fopen($dir.$csvFileName,"w");
       fputcsv($handler,$user);
       /***************************/
       die('Sikeres regisztráció!');
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regisztráció</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
        }

        fieldset {
            margin: 0;
            padding: 0.5em;
            border: 0;
            background: #efefef;
            display: flex;
            flex-flow: column nowrap;
        }

        legend {
            background: #efefef;
            padding: 0.5em;
        }

        .error {
            display: block;
            color: red;
            font-style: oblique;
            font-size: .7em;
            line-height: 1.5em;
        }

        input {
            display: block;
        }
    </style>
</head>
<body>
<form method="post" id="szelveny">
    <fieldset>
        <legend>Személyes adatok</legend>
        <label>
            Név<sup>*</sup>:
            <input type="text" name="name" id="name" value="<?php echo checkValue('name') ?>">
            <?php echo hibaKiir('name'); ?>
        </label>
        <label>
            Email<sup>*</sup>:
            <input type="text" name="email" id="email" value="<?php echo checkValue('email'); ?>"
                   placeholder="email@cim.hu">
            <?php echo hibaKiir('email'); ?>
        </label>
        <label>
            Kor<sup>*</sup>:
            <input type="number" name="age" id="age" value="<?php echo checkValue('age'); ?>"
                   placeholder="18">
            <?php echo hibaKiir('age'); ?>
        </label>
    </fieldset>
    <button>Regisztrálok</button>
</form>
</body>
</html>
