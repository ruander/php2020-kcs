<?php
//@todo: filekezelés native átnézése: fopen, fread, fwrite, fclose
//itt: https://www.php.net/manual/en/ref.filesystem.php

//mappaműveletek
$dir="teszt/";
//mappa létének vizsgálata: is_dir(dir)
if(!is_dir($dir)){//ha nincs mappa
    echo "nincs mappa: $dir";
    //hozzuk létre
    mkdir($dir,0755, true);//jogosultságokról: https://www.php.net/manual/en/function.chmod.php (0644)
}else{//van mappa
    echo "van mappa: $dir";
}
//file kezelése a mappában
$fileName = "tesztszovegfile.txt";
//nézzük meg létezik-e már ilyen file a teszt/ mappában
if(!is_file($dir.$fileName)){//nincs még ilyen file
    echo "<br>Nincs ilyen file: $dir$fileName";
    $content = "Ez egy szöveg file, nem gyakorlat....";
    //hozzuk létre a filet a tartalommal - https://www.php.net/manual/en/function.file-put-contents.php
    file_put_contents($dir.$fileName, $content);

}else{
    //van ilyen file
    echo "<br>van már ilyen file: $dir$fileName";
    //olvassuk be a tartalmát egy változóba
    $readContent = file_get_contents($dir.$fileName);
    echo "<br>ez a tartalma<div style='padding:15px;background: #ededed;border: 1px solid red;'>$readContent</div>";
}
//itt biztosan létezik a fileName fileunk
$newContent = PHP_EOL."Új tartalom...";//PHP_EOL a rendszer sortörést jelenti azaz ami utána jön új sorban lesz
//echo file_get_contents('https://ruander.hu');//url-t is kezel
//irjuk az új tartalmat a fileba
file_put_contents($dir.$fileName,$newContent, FILE_APPEND);
//olvassuk vissza ismét
$readContent = file_get_contents($dir.$fileName);
echo "<br>Új tartalom után ez a tartalma:<div style='padding:15px;background: #ededed;border: 1px solid red;'>$readContent</div>";
