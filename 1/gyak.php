<?php
//pure php file, nem fogjuk zárni a php taget
//echo "Helo";

//egy adott oldalhosszúságú négyzet területének kiszámítása
//elvárt működés: egy 5m oldalhosszúsú négyzet területe 25m2
$a = 4.6;
$terulet = $a*$a;
echo "<div>egy $a m oldalhosszúságú négyzet területe $terulet m<sup>2</sup></div>";
echo '<div>egy $a m oldalhosszúságú négyzet területe $terulet m<sup>2</sup></div>';
echo "<div>egy \$a m oldalhosszúságú négyzet területe \$terulet m<sup>2</sup></div>";// operátor: escape: \ -> kilépteti a következő nyelvi elemet a végrehajtásból
echo '<div>egy '.$a.' m oldalhosszúságú négyzet területe '.$terulet.' m<sup>2</sup></div>';// operátor: konkatenátor .

//2. Írjon egy php programot, amely kiszámolja és kiírja a 2 m sugarú gömb térfogatát. A gömb térfogata V=4/3 r3pi.
$r = 2;//sugár
$terfogat = 4/3*pow($r,3)*pi();// hatványozás pow($r,3) a $r*$r*$r helyett ; négyzetgyök sqrt()
echo "<div>egy $r m sugarú gömb térfogata $terfogat m<sup>3</sup></div>";

//Írjon egy php programot, amely kiszámolja és kiírja a megadott testmagasság alapján az ideális testsúly értékét. Az ideális testsúly kiszámításának képlete:
//T=0.9*(M-100), ahol M a cm-ben megadott testmagasság.
$magassag = 170;
$idealisTestsuly = 0.9 * ($magassag - 100);
echo "<div>egy $magassag cm magasságú ember ideális testsúlya: $idealisTestsuly kg</div>";

//9.Írjon egy php programot, amellyel a Google szóra egy linket állít elő és a link a www.google.com oldalra mutat.
echo '<div><a href="https://google.com">Google</a></div>';
// @todo: hf: 11es feladatig a hazi-feladatok.txt