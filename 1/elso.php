<!doctype html>
<html lang="hu">
 <head>
  <title>első php fileom</title>
  <meta charset="utf-8">
 </head>
 <body>
 <pre>
 <?php 
 //egysoros php komment
 /*
 több soros 
 PHP komment
 */
 //kiírások a fileba
 print "Hello World<br>";//php utasítás: pl: print, php utasítás végén kötelező a ;
 echo 'Hello World<br>';//operátorok: '' és "" , string határolók
 
 //változó típusok - primitívek
 $egeszSzam = 5; // $ -> változó operátor; nem kezdődhet számmal, case sensitive, spec karakter ne legyen benne; = -> értékadó operátor ; ennek a változónak integer a tipusa vagy int
 $logikai = true;// bool vagy boolean tipus
 $szoveg = 'Ez egy szöveg, ami lehet <b>html</b> is.';// string tipus
 $lebego_pontos = 7/8;// floating point vagy float tipus
 //változók kiírása a fileba (érték)
 echo $egeszSzam;
 echo "
           ";//a pre tag miatt ez így is jelenik meg
 //műveletek a változókkal
 $eredmeny = $egeszSzam*34;
 //változó tipusának és értékének 'tesztelése' - csak fejlesztés alatt
 var_dump($eredmeny);//var_dump() php eljárás; function; művelet; függvény; https://www.php.net/manual/en/function.var-dump
 
 var_dump($logikai,$lebego_pontos,$szoveg);
 //Dinamikus változótartalom
 $dobas = rand(1,6);//alapból: 0 - getrandmax()
 echo "A dobás eredménye:";
 echo $dobas;
 ?>
 </pre>
 <!-- html megjegyzés -->
Hello World!
 </body>
</html>