<?php
require "functions.php";//saját eljárások
require "config.php";//erőforrások
/*átkerült a konfigba
$huzasok_szama = 5;//ennyi szám kell
$limit = 90;//1- limitig van a számok alső és felső határa
*/
if (!empty($_POST)) {
    $hiba = [];
    //név min 3 karakter
    $nev = filter_input(INPUT_POST, 'name');
    if (mb_strlen($nev, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Név túl rövid!</span>';
    }
    //email kötelező és email formátum
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Nem megfelelő formátum!</span>';
    }
    //tippek ellenőrzése
    //args on keresztül állítjuk be a szűrést a tippekre
    $args = [
        'tippek' => [
            'filter' => FILTER_VALIDATE_INT,
            'flags' => FILTER_REQUIRE_ARRAY,
            'options' => [
                'min_range' => 1,
                'max_range' => $limit
            ]
        ]
    ];
    $tippek_szures = filter_input_array(INPUT_POST, $args);
    //var_dump('<pre>', $tippek_szures);//hibakereséshez
    //szűrt tömböt bejárva kialakíthatjuk a hibatömb elemeit
    foreach ($tippek_szures['tippek'] as $k => $v) {

        //echo "<br>$v";//hibakereséshez
        if (false === $v) {//ha az adott kulcson falset találunk akkor az a szűrésen elbukott, így a hiba tömb hasonló szerkezetre megkapja a hiba üzenetet
            //die('falset találtam');//hibakereséshez tettük be
            $hiba['tippek'][$k] = '<span class="error">Nem megfelelő formátum!</span>';
        }
    }
    //var_dump('<pre>', $hiba,'</pre>');
    //a tippek ismétlődésének kiszűrése
    //eredeti tippek újraszűrése opciók nélkül
    $args = [
        'tippek' => [
            'flags' => FILTER_REQUIRE_ARRAY
        ]
    ];
    $tippek = filter_input_array(INPUT_POST, $args);//ez minden tipp elem
    $eredeti_tippek = $tippek['tippek'];//ezek már csak a tippek
    $egyedi_tippek = array_unique($eredeti_tippek);//ebből az ismétlődéseket kivesszük
    //bejárjuk az eredeti tippeket és megnézzük hogy szerepel e a kulcsa az adott tippnek az egyedi tippek között, mert ha nem akkor ismétlődés miatt kikerült belőle
    foreach ($eredeti_tippek as $k => $v) {
        if (!array_key_exists($k, $egyedi_tippek)) {//ha nincs ilyen kulcs
            $hiba['tippek'][$k] = '<span class="error">Már szerepelt!</span>';
        }
    }
    //var_dump('<pre>' , $eredeti_tippek , $egyedi_tippek , '</pre>');
    if (empty($hiba)) {
        //nincs hiba
        //írjuk ki fileba az adatokat
        //1. hozzuk létre az adathalmazt
        $data = [
            'nev' => $nev,
            'email' => $email,
            'tippek' => $tippek_szures['tippek']
        ];
        //var_dump('<pre>' ,$data);
        //2. átalakítjuk a tároláshoz, most json formátumba
        $dataToFile = json_encode($data);
        var_dump('<pre>' ,$dataToFile);
        //3. fileba írás lépései, ahhoz hogy egy tippek/lotto-5-90.json adatfileba helyezzük
        //$dir = 'tippek/';//átkerült a configba
        //3.a létezik e a mappa
        if(!is_dir($dir)){
            mkdir($dir,0755,true);
        }
        $fileName = 'lotto-'.$huzasok_szama.'-'.$limit.'.json';//a cél tehát a $dir.$fileName
        //3b megnézzük van e már ilyen file és ha van beolvassuk a tartalmát
        $allTipp = [];//itt lesznek a beolvasott tippek, ha vannak, de itt biztosítjuk hogy legyen egy üres tömb
        if(file_exists($dir.$fileName)){
            $fileContent = file_get_contents($dir.$fileName);
            $allTipp=json_decode($fileContent,true)?:[];//ez az összes eddigi tipp, ha van, ha nem sikerült a
            /*
            if(condition){
                cond true
            }else{
                cont false
            }
            condition ? true : false
            condition ? $a=5 : $a=10 //ezt így ne, helyette
            $a = condition ? 5 : 10;
            //
            $a = condition?:false - ebben az esetben a condition teljesülése esetén maga a condition eredménye lesz az a
            */
        }


        //hozzáadjuk az eddigi tippsorokhoz a most elkészítettet
        array_push($allTipp,$data);
        //most alakítjuk át a json formátumba a már új elemmel együtt
        $dataToFile = json_encode($allTipp);

        //4. kiírjuk az adatokat a kívánt file-ba
        file_put_contents($dir.$fileName,$dataToFile);
    }
}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottójáték</title>
    <style>
        html, body {
            margin: 0;
            padding: 0;
        }

        fieldset {
            margin: 0;
            padding: 0.5em;
            border: 0;
            background: #efefef;
            display: flex;
            flex-flow: column nowrap;
        }

        legend {
            background: #efefef;
            padding: 0.5em;
        }

        .error {
            display: block;
            color: red;
            font-style: oblique;
            font-size: .7em;
            line-height: 1.5em;
        }

        input {
            display: block;
        }
    </style>
</head>
<body>
<h1>Lottójáték</h1>
<form method="post" id="szelveny">
    <fieldset>
        <legend>Személyes adatok</legend>
        <label>
            Név<sup>*</sup>:
            <input type="text" name="name" id="name" value="<?php echo checkValue('name') ?>">
            <?php echo hibaKiir('name'); ?>
        </label>
        <label>
            Email<sup>*</sup>:
            <input type="text" name="email" id="email" value="<?php echo checkValue('email'); ?>"
                   placeholder="email@cim.hu">
            <?php echo hibaKiir2('email'); ?>
        </label>
    </fieldset>
    <fieldset>
        <legend>tippsor</legend>
        <?php
        //ciklus a tippmezőknek
        for ($i = 1; $i <= $huzasok_szama; $i++) {
            echo '<label>
            Tipp ' . $i . '<sup>*</sup>:
            <input type="text" name="tippek[' . $i . ']" id="tipp-' . $i . '"
             value="' . checkValue2('tippek', $i) . '">';//label nyitás input kiírás
            //echo '<pre>'.var_export(checkValue2('tippek',$i),true).'</pre>';
            echo hibaKiir2('tippek', $i);//hibakiírás
            echo '</label>';
        }
        ?>
    </fieldset>
    <button>Tippsor beküldése</button>
</form>
</body>
</html>