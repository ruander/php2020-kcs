<?php
require "config.php";//erőforrások
//filenév kialakítása mappa utvonallal együtt, az adatok a config fileból jönnek
$fileName = $dir.'lotto-'.$huzasok_szama.'-'.$limit.'.json';//ugyanaz az elv mint tároláskor
if(file_exists($fileName)){//ha létezik a file
    $fileContent = file_get_contents($fileName);//betöltjük a tartalmát
    $tippekTomb = json_decode($fileContent,true);//visszalakítjuk tömbbé
    if(is_array($tippekTomb)){//ha a decode eredménye tömb->
        //var_dump($tippekTomb);
        $i=1;//ez lesz a sorszám
        $output='<ul>';
        foreach($tippekTomb as $tippSor){
            $output.='<li><b>'.$i.'</b> | nev:'.$tippSor['nev'].',email:'.$tippSor['email'].', tippek:'.implode(';',$tippSor['tippek']).'</li>';
            $i++;//sorszám növelése
        }
        $output .= '</ul>';
    }
    echo $output;
}