<?php
/**
 * hibák kiírása
 * @param $inputField | string
 * @return mixed (false | STRING)
 */
function hibaKiir($inputField){
    global $hiba;//hogy lássa az eljárás, ill ha nincs deklarálja
    settype($hiba,'array');//$hiba tipusa legyen tömb - tipus kiolvasása: gettype(var) | typecasting
    //ha létezik a hibatömb inputField nevű eleme akkor visszatérünk vele | string
    if( array_key_exists($inputField,$hiba)){
        return $hiba[$inputField];
    }
    return false;
}

//hibakiír másik verzió a tippek tömb hiba miatt
function hibaKiir2($inputField, $tippKey = false){
    global $hiba;//hogy lássa az eljárás, ill ha nincs deklarálja
    settype($hiba,'array');//$hiba tipusa legyen tömb - tipus kiolvasása: gettype(var) | typecasting
    //ha létezik a hibatömb inputField nevű eleme akkor visszatérünk vele | string
    if( array_key_exists($inputField,$hiba)){

        if(is_array($hiba[$inputField])){//ha tömb akkor tippről lesz szó
            if(array_key_exists($tippKey,$hiba[$inputField])){//ha létezik abban a tömbben a tipp sorszáma akkor van rajta hiba és térjünk vissza vele
                return $hiba[$inputField][$tippKey];
            }
        }else {
            //különben sima mezőként dolgozzuk fel
            return $hiba[$inputField];
        }
    }
    return false;
}
/**
 * Űrlapmezők value értékeinek visszaírásához segédeljárás
 * @param $inputField
 * @return mixed (null | string)
 */
function checkValue($inputField){
    return filter_input(INPUT_POST,$inputField);
}/**
 * Űrlap TIPPMEZŐK value értékeinek visszaírásához segédeljárás
 * @param $inputField | ezen a kulcson van a tipp halmaza
 * @return mixed (null | string)
 */
function checkValue2($inputField,$element){
    //alapszűrés az értékek visszaadására
    $args = [
        $inputField => [
            'flags' => FILTER_REQUIRE_ARRAY
        ]
    ];
    $retArray = filter_input_array(INPUT_POST, $args);//ez minden tipp elem
    return $retArray[$inputField][$element];//amelyik elem éppen kell belőle
}