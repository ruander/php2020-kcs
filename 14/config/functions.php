<?php
//saját eljárások gyüjteménye
/**
 * Admin menü összeállító eljárás
 * @param array $adminMenu
 * @return string
 */
function adminMenu($adminMenu = []){
    $ret = '<ul>';
    //menüpontok
    foreach($adminMenu as $m_id => $menu){
        $ret .= '<li><a href="?p='.$m_id.'">'.$menu['title'].'</a></li>';
    }
    $ret .= '</ul>';

    return $ret;

}

/**
 * cikkek lekérése
 * @param int $start
 * @param int $articleQty
 * @return string
 */
function getArticles($start = 0, $articleQty = 3){
    date_default_timezone_set ('Europe/Budapest');
    global $link, $honapok;
    setlocale(LC_ALL, 'hun');
    $image_dir = 'uploads/';
    $qry = "SELECT * FROM articles WHERE status = 1 ORDER BY publish_date DESC LIMIT $start,$articleQty";
    $result = mysqli_query($link,$qry) or die(mysqli_error($link));
    $ret = ' <ul class="posts">';
    while($row = mysqli_fetch_assoc($result) ){
        $excerpt = implode(' ', array_slice(explode(' ', $row['lead']), 0, 15))."\n";
        $image = $image_dir.$row['id'].'/thumb/'.$row['seo_title'].'.jpg';//thumbnail

        if(file_exists($image)){
            $imgHtml = '<img src="'.$image.'" alt="'.$row['title'].'">';
        }else{
            $imgHtml = '<img src="https://picsum.photos/480/320?a" alt="'.$row['title'].'- generált" title="'.$row['title'].'- generált">';
        }
        $datum = strtotime($row['time_created']);
        $ret .='<li class="post">
                <!--(.head>.comments{25})+(a.title{lorem ipsum title}+p>lorem25)+.footer>.author+time{dátum}-->
                <div class="head">
                    <div class="comments">25</div>
                    '.$imgHtml.'</div>
                <a href="#" class="title">'.$row['title'].'</a>
                <p>'.$excerpt.'</p>
                <div class="footer">
                    <div class="author"><img alt="'.$row['author'].'" src="https://picsum.photos/40?c">'.$row['author'].'</div>
                    <time datetime="'.$row['time_created'].'">'.date('Y. ',$datum). $honapok[ date('n',$datum)] .date(' d.',$datum).'</time>
                </div>
            </li>';

    }
    $ret .='</ul>';

    return $ret;
}
/**
 * mappa ell/készítés
 * @param $dir
 */
function checkDir($dir){
    //mappa ellenőrzése
    if (!is_dir($dir)) {
        mkdir($dir, 0755, true);
    }
}
/**
 * űrlap értékek kiválasztása - POST | db_adat | semmi - hibás űrlap | updatehez lekért adatok | uj felvitel első megjelenítés
 * @param $field - input mező neve
 * @param array $rowData - adatbázisból lekért adatok tömbje
 * @return string
 */
function valueCheck($field, $rowData = [], $mode = 'text')
{
    $ret = '';
    switch ($mode) {
        case 'checkbox':
            if (filter_input(INPUT_POST, $field)) {//ha ki volt pipálva akkor létezik a postban, ha nem akkor nem is létezik
                $ret = 'checked';
            } elseif (array_key_exists($field, $rowData) and $rowData[$field] == 1) {
                $ret = 'checked';
            }
            break;
        default:
            if (filter_input(INPUT_POST, $field) !== null) {//ha jön adat a postból az adott kulcson akár üres string is
                $ret = filter_input(INPUT_POST, $field);
            } elseif (array_key_exists($field, $rowData)) {
                $ret = $rowData[$field];
            }
    }
    return $ret;
}
/**
 * eljárás a valuek visszaadására
 * @param $key elem neve a postban
 * @param string $dbData adatbázisból kapott érték
 * @return mixed|string //post adat, db adat vagy semmi
 */
function checkValue($key, $dbData = '')
{
    $postData = filter_input(INPUT_POST, $key);//ha létezik ilyen post elem
    if ($postData !== null) return $postData;// akkor visszaadjuk azt

    return $dbData;//különben visszaadjuk a db adatot ami, ha nincs akkor üres string
}

/**
 * CheckBox default checker
 * @param $key - input name
 * @param string $dbData - data from database (0-1)
 * @return 'checked' | empty string
 */
function checkBox($key, $dbData = '')
{
    if (!empty($_POST)) {
        if (filter_input(INPUT_POST, $key)) return "checked";
        return '';
    }
    if ($dbData) return "checked";
    return '';
}
/**
 * adatbázisból kapott adatok meglétének ellenőrzésére készített eljárás
 * @param $array | ebben kellenek legyenek az adatok
 * @param $key | ezt a kulcsot keressük az adattömbben
 * @return mixed|null
 */
function hasData($array,$key){
    if(array_key_exists($key,$array)){
        return $array[$key];
    }
    return null;
}
//hiba kiírásra
function hibaKiir($key, &$hiba){//hiba változó referenciaátadása
    //ha nem létezik a hiba tömb, (array_key__existnek az kell!) akkor létrehozunk üreset, ha létezik akkor nem változtatjuk
    $hiba = $hiba?:[];
    //ha létezik az adott kulcs akkor visszaadjuk a rajta található értéket
    if(array_key_exists($key,$hiba)){
        return $hiba[$key];
    }
    return;
}
//kupon megjelenítéshez '-' hozzáadása a tokenhez
function hyphenate($str)
{
    return implode("-", str_split($str, 6));
}
/*
 * string kezelő eljárások
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő - eltávolítása
    return $str;
}
/**
 * Admin beléptető eljárás
 * @return bool
 */
function login()
{
    global $link, $secret_key;//db link és titk. kulcs
    //email/jelszó ellenőrzés
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $pass = filter_input(INPUT_POST, 'pass');
    //hash lekérése db ből, ha van
    $qry = "SELECT id,username,password FROM admins WHERE email = '$email' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);
    if ($row) {
        $testHash = $row['password'];
        if (password_verify($pass, $testHash)) {
            $sid = session_id();
            $_SESSION['id'] = $sid;
            $_SESSION['userdata'] = [
                'id' => $row['id'],
                'username' => $row['username']
            ];
            //$secret_key, user_id, session_id
            $spass = md5($sid . $row['id'] . $secret_key);
            $stime = time();
            //beragadt belépések takarítása a mf azonosítóra
            mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid' LIMIT 1");
            //sessions táblába elmentjük a bejelentkezésből és környezetből generált adatokat és az időt amikor történt
            $qry = "INSERT INTO sessions(sid,spass,stime) VALUES ('$sid','$spass','$stime')";
            mysqli_query($link, $qry) or die(mysqli_error($link));
            //echo '<pre>'.var_export($_SERVER,true).'</pre>';
            //sikeres ellenőrzés - mehetünk az indexre
            return true;
        }
    }
    return false;
}

/**
 * Atuhentikáció a belépés és környezeti adatok alapján
 * @return bool
 */
function auth()
{
    global $link, $secret_key;
    $now = time();
    $expired = $now - 900; //most 15p 60*15
    $sid = session_id();
    $spass = md5($sid . $_SESSION['userdata']['id'] . $secret_key);
    //echo '<pre>' . var_export($spass, true) . '</pre>';
//öntisztítás
    mysqli_query($link, "DELETE FROM sessions WHERE stime < '$expired'");
//kérjük le db ből spass alapján a sid et és nézzük meg visszakaptuk-e
    $qry = "SELECT sid FROM sessions WHERE spass = '$spass' AND stime > $expired LIMIT 1 ";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
    if ($row[0] !== session_id()) {
        //ha nem sikerül akkor rakjuk rendbe a dolgokat :)
        mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid' LIMIT 1");//extra ujralogin error védelem
        unset($_SESSION);
        session_destroy();
        return false;
    } else {
        //stime update
        mysqli_query($link, "UPDATE sessions SET stime = '$now' WHERE sid  = '$sid' LIMIT 1") or die(mysqli_query($link));
        return true;
    }
}