<?php

$secret_key = 'S3creT_%K3Y!';//ált. titkosító string
//admin menüpontok
$adminMenu = [
    0 => [
        'title' => 'Dashboard',
        'module' => 'dashboard',
        'icon' => 'fa fa-dashboard'
    ],
    1 => [
        'title' => 'Adminok',
        'module' => 'admins',
        'icon' => 'fa fa-user'
    ],
    2 => [
        'title' => 'Cikkek',
        'module' => 'articles',
        'icon' => 'fa fa-bars'
    ],
];

$moduleExt = '.php';//modulefileok kiterjesztése
$honapok = Array( "", "január", "február", "március",
    "április", "május", "június",
    "július", "augusztus", "szeptember",
    "október", "november", "december");