<?php
//hf: a cikkekhez lehessen képet feltölteni, a célpont public/images/{cikkId}/képnév=seotitle+jpg
//https://www.php.net/manual/en/mysqli.insert-id.php - createkor hasznos
//listázáskor legyen kicsi kép a listában egy oszlopban, és a z urlapon jelenjen meg ha van feltöltött kép
//legyen célmappa most az uploads/
$dir = "../public/uploads/";// public/uploads/{cikk:id}/hircime.jpg .../thumb/hircime.jpg
//mappa ellenőrzése
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}

$validTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];//$_FILES['file']['type']
//var_dump('<pre>',$_FILES);

//file feltöltés sikeres volt
if ($_FILES['file']['error'] == 0 && is_uploaded_file($_FILES['file']['tmp_name'])) {
    //képformátum ellenőrzése
    $info = getimagesize($_FILES['file']['tmp_name']);//ez nem átverhető,  mime tartalmazza a típust pontosan ha kép, egyébként az egész getimagesize false, ha nem kép

    if ($info !== false) {//csak akkor megyünk tovább, ha kép a feltöltött file
        if (!in_array($info['mime'], $validTypes)) {
            $hiba['file'] = 'Nem megengedett formátum.';
        } else {
            //biztos hogy kép, elkezdhetünk vele dolgozni.
            //képméret beállítása (max fullhd) - resize - méretarányos kicsinyítés
            $src_w = $info[0];
            $src_h = $info[1];
            //álló v fekvő kép, mert most max 600px engedünk a nagyobbik méretre
            //képarány
            $ratio = $src_w / $src_h;//<1 álló, >1 fekvő, 1 négyzet
            if ($ratio > 1) {
                //max w 600
                $dst_w = 600;
                $dst_h = round($dst_w / $ratio);// <600
            } else {
                //max h 600
                $dst_h = 600;
                $dst_w = round($ratio * $dst_h);// <600
            }
            //vászon amire dolgozunk a mem-ban
            $canvas = imagecreatetruecolor($dst_w, $dst_h);
            //forráskép mem-ba //tipusfüggő
            $src_image = imagecreatefromjpeg($_FILES['file']['tmp_name']);
            imagecopyresampled($canvas, $src_image, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);

            //ez a file látszódjon képnek
            //header('content-type:image/jpeg');
            //vászon megjelenítése a mem-ból
            //fileba: uploads/kép eredeti neve
            $filename = $dir . $_FILES['file']['name'];
            imagejpeg($canvas, $filename, 100);

            //exit();
            //thumbnail elkészítése 150x150 - crop-resize
            $dst_w = $dst_h = 150;
            if ($ratio > 1) {
                //eltolás x tengelyen a kicsinyített méretkülönbség felével
                $target_h = 150;
                $target_w = 150 * $ratio;
                $dst_y = 0;
                $dst_x = round(($target_w - $dst_w) / 2);
            } else {
                //eltolás y tengelyen
                $target_w = 150;
                $target_h = 150 / $ratio;
                $dst_x = 0;
                $dst_y = round(($target_h - $dst_h) / 2);
            }
            $canvas = imagecreatetruecolor($dst_w, $dst_h);
            imagecopyresampled($canvas, $src_image, -$dst_x, -$dst_y, 0, 0, $target_w, $target_h, $src_w, $src_h);
            //header('content-type:image/jpeg');
            $filename = $dir . "thumb-" . $_FILES['file']['name'];
            imagejpeg($canvas, $filename, 60);
            //exit();
            //takarítás a memóriából
            imagedestroy($canvas);
            imagedestroy($src_image);
        }
    } else {
        $hiba['file'] = 'Nem képet töltöttek fel';
    }
} else {
    $hiba['file'] = 'Hiba a feltöltés során!';
}
