<?php
//pure php
//require "../../config/connect.php";//db
//require "../../config/functions.php";//saját eljárások
//modulként működhet csak, ha külön futtatják nem kell futnia pl ha nincs $link
if(!isset($link)){
    header('location:../index.php');
    exit();
}
//teszt lekérés - gyakorlás
/**
 * Erőforrások
 */
//output, ide jönnek a kiírandó elemek
$output = '';
//státuszok
$status = [
    0 => 'inaktív',
    1 => 'aktív',
    2 => 'törölt'
];
//url paraméterek
$muvelet = filter_input(INPUT_GET, 'act') ?: 'list';//művelet tipusa
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);//azonosító amivel szeretnénk a műveletet elvégezni
$hiba = [];//üres hiba tömb
//hibakezelés
if (!empty($_POST)) {
    //username min 3 karakter
    $username = mysqli_real_escape_string($link, trim(filter_input(INPUT_POST, 'username')));//mysql injection elleni védelem és trim a spacek eltávolítására
    if (mb_strlen($username, "utf-8") < 3) {
        $hiba['username'] = '<span class="error">Név min. 3 karakter!</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás adat!</span>';
    } else {
        //létezik e már?
        $qry = "SELECT id FROM admins WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $checkEmailId = mysqli_fetch_row($result);
        //$tid tartalmazza az updatelni kivánt admin azonosítóját
        //adatbázisból a $checkEmailId[0] tartalmazza ha volt ilyen, ha nem akkor az egész row NULL
        var_dump($tid, $checkEmailId);
        if ($checkEmailId && $checkEmailId[0] != $tid) {
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }

    //jelszó min 6 karakter
    $pass = filter_input(INPUT_POST, 'pass');
    $repass = filter_input(INPUT_POST, 'repass');
    if ($muvelet == 'create' || $muvelet == 'update' && $pass != '') {
        if (mb_strlen($pass, "utf-8") < 6) {
            $hiba['pass'] = '<span class="error">Jelszó min. 6 karakter!</span>';
        } elseif ($pass !== $repass) {
            $hiba['repass'] = '<span class="error">A jelszavak nem egyeztek!</span>';
        } else {
            $pass = password_hash($pass, PASSWORD_BCRYPT);//jelszó elkódolása
        }
    }
    //státusz
    $status = filter_input(INPUT_POST, 'status') ? 1 : 0;
    if (empty($hiba)) {//nincs hiba sehol
        $now = date('Y-m-d H:i:s');//idő
        //adatok tisztázása
        $admin = [
            'username' => $username,
            'email' => $email,
            'password' => $pass,
            'status' => $status,
            'time_created' => $now
        ];

        if ($muvelet == 'create') {//új felvitel adatbázisba
            /*$qry = "INSERT INTO admins(`username`,`email`,`password`,`status`,`time_created`)
                    VALUES('$username','$email','$pass','$status','$now')";*/
            $qry = "INSERT INTO admins(`" . implode("`,`", array_keys($admin)) . "`)
                VALUES('" . implode("','", $admin) . "')";  //kérés összeállítása segédtömbbel
        } else {//update
            $admin['time_updated'] = $now;
            // @todo: password?
            $setPass = ($pass) ? ", `password` = '" . $admin['password'] . "' " : '';

            $qry = "UPDATE admins SET 
                    `username` = '" . $admin['username'] . "',
                    `email` = '" . $admin['email'] . "',
                    `status` = '" . $admin['status'] . "',
                    `time_updated` = '" . $admin['time_updated'] . "' 
                    $setPass
                    WHERE id = $tid LIMIT 1";
        }

        mysqli_query($link, $qry) or die(mysqli_error($link));//kérés futtatása
        //visszairányítunk listázásra
        header('location:' . $baseUrl);
        exit();
    }
}


//switch a műveletek szétválasztására és az output elemek kialakítására
switch ($muvelet) {
    case 'delete':
        $qry = "DELETE FROM admins WHERE id = '$tid' LIMIT 1";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //visszairányítunk listázásra
        header('location:' . $baseUrl);
        exit();
        break;
    case 'update':
        $qry = "SELECT id,username,email,status FROM admins WHERE id = '$tid' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $admin = mysqli_fetch_assoc($result);
        //echo '<pre>' . var_export($admin, true) . '</pre>';
        $buttonTitle = 'Módosítás';
    //break;
    case 'create':
        //submit gomb felirata módosítás vagy új felvitel
        $buttonTitle = ($muvelet == 'update')?'Módosítás':'Új felvitel';
        //ha van adat betöltjük az űrlapba
        $admin = isset($admin) ? $admin : [];//ha van legyen önmaga, ha nincs akkor üres tömb
        $form = '<a href="'.$_SERVER['PHP_SELF'].'">&lt;- mégse</a><br>
                 <form method="post">';
        //név
        $form .= '<label>
                        Felhasználónév:
                        <input type="text" name="username" placeholder="Petike" value="' . valueCheck('username', $admin) . '">';
        //mezőhiba
        if (array_key_exists('username', $hiba)) {
            $form .= $hiba['username'];
        }
        $form .= '</label>';
        //email
        $form .= '<label>
                        email:
                        <input type="text" name="email" placeholder="email@cim.hu" value="' . valueCheck('email', $admin) . '">';
        //mezőhiba
        if (array_key_exists('email', $hiba)) {
            $form .= $hiba['email'];
        }
        $form .= '</label>';
        //jelszó1
        $form .= '<label>
                        Jelszó:
                        <input type="password" name="pass" value="">';
        //mezőhiba
        if (array_key_exists('pass', $hiba)) {
            $form .= $hiba['pass'];
        }
        $form .= '</label>';
        //jelszó2
        $form .= '<label>
                        Jelszó mégegyszer:
                        <input type="password" name="repass" value="">';
        //mezőhiba
        if (array_key_exists('repass', $hiba)) {
            $form .= $hiba['repass'];
        }
        $form .= '</label>';
        //státusz
        $checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';
        $form .= '<label>
                        <input type="checkbox" name="status" value="1" ' . valueCheck('status', $admin, 'checkbox') . '> aktív?
                    </label>';
        //submit gomb
        $form .= '<button>'.$buttonTitle.'</button>
                </form>';
        $output .= $form;
        break;
    default:
        $qry = "SELECT id,username,email,status FROM admins";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));

        $table = '<a href="'.$baseUrl.'&amp;act=create">új felvitel</a>
           <table border="1">
            <tr>
             <th>id</th>
             <th>név</th>
             <th>email</th>
             <th>státusz</th>
             <th>művelet</th>
            </tr>';
        while (null !== $row = mysqli_fetch_assoc($result)) {
            $table .= '<tr>
             <td>' . $row['id'] . '</td>
             <td>' . $row['username'] . '</td>
             <td>' . $row['email'] . '</td>
             <td>' . $status[$row['status']] . '</td>
             <td><a href="'.$baseUrl.'&amp;act=update&amp;id=' . $row['id'] . '">módosít</a> | <a href="'.$baseUrl.'&amp;act=delete&amp;id=' . $row['id'] . '">töröl</a></td>
            </tr>';
        }
        $table .= '</table>';
        //tábla -> output
        $output .= $table;
        break;//kilép a switchből a következő sorba ami utána van
}


//kiírás egy lépésben majd az indexben


$moduleStyle = '<style>
    label,
    input:not([type="checkbox"]) {
        display: block;
        margin: 5px 0;
    }
    .error {
    color:red;
    font-style: italic;
    font-size: 11px;
    }
</style>';//kiírás az indexben
