<?php
//Adminisztrátorok kezelése
//erőforrások
$action = filter_input(INPUT_GET, 'action') ?: 'read'; //művelet tipusa urlből , ha nincs read, azaz listázás
$tid = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) ?: null;
//var_dump($_POST);
$dir = "../public/uploads/";// public/uploads/{cikk:id}/hircime.jpg .../thumb/hircime.jpg

$image_sizes = [
    'thumb' => [
        'w' => 480,
        'h' => 320
    ],
    'article' => [
        'w' => 1200,
        'h' => 650
    ],
];
$validTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];//$_FILES['file']['type']
$output = '';//ez lesz a kiírandó
if (!isset($link)) {//önállóan akarják futtatni a filet, vagy gond $link változóval
    header('location:../index.php');
    exit();
}
$db_table = 'articles';//ez lesz a db tábla amibe az adatok lesznek
//űrlap adatok feldolgozása/hibakezelés ha kell
if (!empty($_POST)) {
    $hiba = [];
    //cím kötelező
    $title = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'title'));
    //elkészítjük a seo címet
    $seo_title = ekezettelenit($title);
    if (!$title) {
        $hiba['title'] = '<span class="error">kötelező kitölteni!</span>';
    }
    //szerző, ha nincs adjunk
    $author = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'author')) ?: 'Nameless One';
    //status
    $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT) ? 1 : 0;

    //lead
    $lead = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'lead'));
    //content
    $content = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'content'));
    //publish date: @todo hf : a publish date mező egy calendar  js elem legyen (datetime picker)
    //var_dump($_FILES);
    //kép hibakezelése ha van feltöltött file
    if ($_FILES['file']['error'] == 0 && is_uploaded_file($_FILES['file']['tmp_name'])) {
        //képformátum ellenőrzése
        $info = getimagesize($_FILES['file']['tmp_name']);//ez nem átverhető,  mime tartalmazza a típust pontosan ha kép, egyébként az egész getimagesize false, ha nem kép
        if ($info === false || !in_array($info['mime'], $validTypes)) {//csak akkor megyünk tovább, ha kép a feltöltött file
            $hiba['file'] = '<span class="error">Hiba a kép feltöltése közben!</span>';
        }
    }
    if (empty($hiba)) {
        //adatok tisztázása
        $now = date('Y-m-d H:i:s');//datetime

        $data = [
            'title' => $title,
            'seo_title' => $seo_title,
            'status' => $status,
            'author' => $author,
            'lead' => $lead,
            'content' => $content
        ];//ezek az adatok mennek a db-be
        //update/crate szétválasztása
        if ($action == 'create') {
            $data['time_created'] = $now;
            //uj cikk felvitele a segédtömb alapján a kulcsok és értékek felhasználásával
            $qry = "INSERT INTO `$db_table` (`" . implode("`,`", array_keys($data)) . "`) 
                    VALUES ('" . implode("','", $data) . "')";
        } else {//update
            $data['time_updated'] = $now;
            $recordSet = [];
            foreach ($data as $key => $value) {
                $recordSet[] = "$key = '$value' ";
            }
            //dd($recordSet);
            $qry = "UPDATE $db_table SET " . implode(',', $recordSet) . " WHERE id = $tid";

        }
        mysqli_query($link, $qry) or die(mysqli_error($link));//insert vagy error
        $cikk_id = $tid ?: mysqli_insert_id($link);
        $dir .= $cikk_id . '/';
        $thumbDir = $dir . 'thumb/';
        checkDir($thumbDir);//mappa készítés ha nincs

        //kép kezelése
        if (is_array($info)) {
            //biztos hogy kép, elkezdhetünk vele dolgozni.
            //képméret beállítása (max fullhd) - resize - méretarányos kicsinyítés
            $src_w = $info[0];
            $src_h = $info[1];
            //álló v fekvő kép, mert most max 600px engedünk a nagyobbik méretre
            //képarány
            $ratio = $src_w / $src_h;//<1 álló, >1 fekvő, 1 négyzet
            if ($ratio > 1) {
                //image_sizes w
                $dst_w = $image_sizes['article']['w'];
                $dst_h = round($dst_w / $ratio);
            } else {
                //image_sizes h
                $dst_h = $image_sizes['article']['w'];
                $dst_w = round($ratio * $dst_h);
            }
            //vászon amire dolgozunk a mem-ban
            $canvas = imagecreatetruecolor($dst_w, $dst_h);
            //forráskép mem-ba //tipusfüggő
            $src_image = imagecreatefromjpeg($_FILES['file']['tmp_name']);
            imagecopyresampled($canvas, $src_image, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);

            //ez a file látszódjon képnek
            //header('content-type:image/jpeg');
            //vászon megjelenítése a mem-ból
            //fileba: uploads/kép eredeti neve
            $filename = $dir . $seo_title . '.jpg';
            imagejpeg($canvas, $filename, 100);

            //exit();
            //thumbnail elkészítése 150x150 - crop-resize
            $dst_w = $image_sizes['thumb']['w'];
            $dst_h = $image_sizes['thumb']['h'];
            if ($ratio > 1) {
                //eltolás x tengelyen a kicsinyített méretkülönbség felével
                $target_h = $dst_h;
                $target_w = $target_h * $ratio;
                $dst_y = 0;
                $dst_x = round(($target_w - $dst_w) / 2);
            } else {
                //eltolás y tengelyen
                $target_w = $dst_w;
                $target_h = $target_w / $ratio;
                $dst_x = 0;
                $dst_y = round(($target_h - $dst_h) / 2);
            }
            $canvas = imagecreatetruecolor($dst_w, $dst_h);
            imagecopyresampled($canvas, $src_image, -$dst_x, -$dst_y, 0, 0, $target_w, $target_h, $src_w, $src_h);
            //header('content-type:image/jpeg');
            $filename = $thumbDir . $seo_title . '.jpg';
            imagejpeg($canvas, $filename, 60);
            //exit();
            //takarítás a memóriából
            imagedestroy($canvas);
            imagedestroy($src_image);
        }
        //visszairányítunk listázásra
        header('location:' . $baseUrl);//átirányítás a modul betöltésre
        exit();
    }
}

//switch a működés szétválasztására
switch ($action) {
    case 'delete':
        //törlés
        if ($tid) {
            mysqli_query($link, "DELETE from $db_table WHERE id = $tid") or die(mysqli_error($link));
        }
        //visszairányítunk listázásra
        header('location:' . $baseUrl);
        exit();
        break;//ez nem kellene de maradjon benn (exit miatt sose jut ide ez az ág)
    case 'update':
        //módosítás
        if ($tid) {
            $qry = "SELECT * FROM $db_table WHERE id = $tid LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $rowData = mysqli_fetch_assoc($result);
            //var_dump($user);
        }
    //break;
    case 'create':
        $rowData = isset($rowData) ? $rowData : [];//ha nincs db adat legyen üres tömb helyette az ellenőrző eljárásaink miatt léteznie kell
        //új felvitel űrlap
        $form = '<form method="post" class="default-form" enctype="multipart/form-data">
            <fieldset>
                <label>Cím<sup>*</sup>
                    <input type="text" name="title" placeholder="bla bla bla" value="' . checkValue('title', hasData($rowData, 'title')) . '">';//űrlap elem értékének visszaírása
        if (isset($hiba['title'])) {//hiba 'befűzése' az űrlap elemhez ha van
            $form .= $hiba['title'];
        }
        $form .= '</label>
                <label>Seo Cím (automatikus)
                    <input type="text" name="seo_title"  value="' . (isset($seo_title) ? $seo_title : hasData($rowData, 'seo_title')) . '" disabled></label>';//űrlap elem értékének visszaírása - ez a mező csak tájékoztatás, nem innen születik meg az adat hanem mindig a titleből generáljuk
//szerző, default-belépett admin neve
        $form .= '</label>
                <label>Szerző
                    <input type="text" name="author"  value="'
            . checkValue('author', hasData($rowData, 'author') ?: $_SESSION['userdata']['username'])
            . '"></label>';
        //bevezető
        $form .= '<label>Bevezető
                    <textarea name="lead" cols="70" rows="5">' . checkValue('lead', hasData($rowData, 'lead')) . '</textarea></label>';
        //tartalom
        $form .= '<label>Tartalom
                    <textarea name="content" cols="70" rows="15">' . checkValue('content', hasData($rowData, 'content')) . '</textarea></label>';

//aktív - inaktív
        $form .= '<label><input type="checkbox" name="status" value="1" ' . checkBox('status', hasData($rowData, 'status')) . ' > aktív?</label>';
        $form .= '</fieldset>';

        $form .= '<fieldset>';
        $form .= '<label>Kep a cikkhez: <input type="file" name="file">';
        $form .= isset($hiba['file']) ? $hiba['file'] : '';
        $form .= '</label>';
        $imageFile = $dir.$tid.'/thumb/'.hasData($rowData, 'seo_title').'.jpg';
        if(file_exists($imageFile)){
            $form .='<img src="'.$imageFile.'" alt="kép">';
        }

        $form .= '</fieldset>';
//form zárása és a gomb
        $form .= '<button class="btn btn-success" type="submit">' . ($action == 'create' ? 'Új Felvitel' : 'Módosítom') . '</button>
</form>';
        $output .= $form;
        break;
    default:
        //lista
        $qry = "SELECT * FROM $db_table";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $table = '<div class="row">
                    <div class="col"><a href="' . $baseUrl . '&amp;action=create" class="btn btn-primary">Új felvitel</a>
                    <table class="table table-striped table-responsive">
                    <tr>
                        <th>id</th>
                        <th>kép</th>
                        <th>cím</th>
                        <th>szerző</th>
                        <th>státusz</th>
                        <th>művelet</th>
                    </tr>';//table nyitás és fejléc
        //sorok
        while ($row = mysqli_fetch_assoc($result)) {
            $table .= '<tr>
                        <td>' . $row['id'] . '</td>
                        <td>';
            $imageFile = $dir.$row['id'].'/thumb/'.hasData($row, 'seo_title').'.jpg';
            if(file_exists($imageFile)){
                $table .='<img src="'.$imageFile.'" alt="kép" width="80">';
            }
            $table .= '</td>
                        <td>' . $row['title'] . '</td>
                        <td>' . $row['author'] . '</td>
                        <td>' . $row['status'] . '</td>
                        <td><div class="actions"> 
                        <a href="' . $baseUrl . '&amp;action=update&amp;id=' . $row['id'] . '"><i class="far fa-edit"></i>M</a> <a href="' . $baseUrl . '&amp;action=delete&amp;id=' . $row['id'] . '"><i class="fas fa-trash-alt"></i>T</a>
                       </div> </td>  
                    </tr>';
        }
        $table .= '</table>
</div></div>';
        $output .= $table;
        break;
}


//kiírás majd az indexben


//styles
$moduleStyle = "<style>
.registration-form fieldset {
display: flex;
flex-flow: column nowrap;
}
label {
    display:block;
}
.error {
    font-style: italic;
    color:red;
}
</style>";

/**
 * CREATE TABLE `articles` ( `id` int(11) UNSIGNED NOT NULL, `title` tinytext NOT NULL, `seo_title` tinytext NOT NULL, `lead` varchar(500) NOT NULL, `content` text NOT NULL, `author` varchar(100) NOT NULL, `status` tinyint(1) UNSIGNED NOT NULL, `publish_date` datetime NOT NULL, `time_created` datetime NOT NULL, `time_updated` datetime NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 */


