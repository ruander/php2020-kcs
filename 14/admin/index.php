<?php
require "../config/connect.php";//db
require "../config/functions.php";//saját eljárások
require "../config/settings.php";//rendszerbeállítások
session_start();//session indítása
//var_dump($_SESSION, session_id());
//kell valami megoldás hogy a felhasználó email/password páros alapján eldöntsem jogosult -e a file megtekintésére aki meg szeretné nézni
//ha kilépni szeretne user akkor beleroncsolunk az auth bármelyik elemébe
if(filter_input(INPUT_GET,'logout') !== null){
    unset($_SESSION['userdata']);
}
$auth = auth();//belépés ellenőrzése
//felh azonosito: $_SESSION['userdata']['id']

if (!$auth) {
    header('location:login.php');
    exit();
}
////////////////////////////////////////
/// //melyik oldal/modul kell:
$o = filter_input(INPUT_GET,'p',FILTER_VALIDATE_INT)?:0;//ha nincs akkor 0 azaz dashboard
$baseUrl = "?p=$o"; //rendszer alap url amiben benne van az aktív oldal azonositó
$output = '';
  $moduleFile ='modules/'.$adminMenu[$o]['module'].$moduleExt;
  //ha létetzik a modulefile, töltsük be
    if(file_exists($moduleFile)){
        //modul betöltése (logika)
        include $moduleFile;
    }else{
        $output = 'Nincs ilyen modul:' . $moduleFile;
    }

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adminisztrációs felület</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="css/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <?php
    if(isset($moduleStyle)){//ha van modulstílus írjuk ide
        echo $moduleStyle;
    }
    ?>
</head>
<body>
<?php
//felhasználó üdvözlés és kilépési lehetőség
$userbar = '<div class="userbar">Üdvözlet kedves ' . $_SESSION['userdata']['username'] . '! | <a href="?logout">kilépés</a></div>';
echo $userbar;
//menu @todo a blank adminLte filebol átemelni a szükséges elemeket, és include-okra szétbontani a navigációt
echo adminMenu($adminMenu);


//output a modulból
echo $output;
?>
<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>

</body>
</html>
