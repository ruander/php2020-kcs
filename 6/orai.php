<?php

echo '<pre>'.var_export($_POST,true).'</pre>';
//űrlapfeldolgozás 2
if(!empty($_POST)){
    $hiba = [];//üres hiba
    //hibakezelés hiba->hibatömbbe
    //név minimum 3 karakter kell legyen spacek nélkül ' ff  '
    $name = filter_input(INPUT_POST,'name');
    $name = trim($name);//sorvégi spacek eltávolitása: trim(mit, pattern), ltrim, rtrim
    if( mb_strlen($name,'utf-8') < 3 ){
        $hiba['name']='<span class="error">Név min. 3 karakter kell legyen!</span>';
    }

    //@todo: email legyen email formátumu | FILTER_VALIDATE_EMAIL 3.paraméternek filter inputba

    //@todo: jelszó min 6 karakter és a két jelszó egyezzen - tilos a trim - tilos a value visszaírás
//jelszó1 hoszz nem oké-e -> hiba?
    //különben a két jelszó egyenlő e ( elseif() ) használata
/*
 if(jelszo1 nemoké){
    hiba a tömbbe
}elseif(két jelszó nem egyezik){
    hiba a tömbbe
}else{
    jelszó jó
}
 */
    if(empty($hiba)){
        //nincs hiba
        die('Űrlapadatok helyesek!');
    }
}//empty post vége

/**
 * hibák kiírása
 * @param $inputField | string
 * @return mixed (false | STRING)
 */
function hibaKiir($inputField){
    global $hiba;//hogy lássa az eljárás, ill ha nincs deklarálja
    settype($hiba,'array');//$hiba tipusa legyen tömb - tipus kiolvasása: gettype(var) | typecasting
    //ha létezik a hibatömb inputField nevű eleme akkor visszatérünk vele | string
    if( array_key_exists($inputField,$hiba)){
        return $hiba[$inputField];
    }
    return false;
}
/**
 * Űrlapmezők value értékeinek visszaírásához segédeljárás
 * @param $inputField
 * @return mixed
 */
function checkValue($inputField){
    return filter_input(INPUT_POST,$inputField);
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regisztráció</title>
    <style>
        h1 {
            text-align: center;
        }
        label {
            margin: 10px;
        }
        .registration-form {
            width: 400px;
            margin: 0 auto;
            display: flex;
            flex-flow: column nowrap;
        }
    </style>
</head>
<body>
<main>
    <h1>Regisztráció</h1>
    <form method="post" class="registration-form">
        <label>Név<sup>*</sup>
            <input type="text" name="name" value="<?php echo checkValue('name') ?>" placeholder="John Doe">
            <?php
            //echo $hiba['name'];//noticeol első betöltéskor
            echo hibaKiir('name');//hiba kiírása saját eljárással
            ?>
        </label>

        <label>Email<sup>*</sup>
            <input type="text" name="email" value="" placeholder="email@cim.hu">
        </label>
        <label>Jelszó<sup>*</sup>
            <input type="password" name="pass" value="">
        </label>
        <label>Jelszó mégegyszer<sup>*</sup>
            <input type="password" name="repass" value="">
        </label>

        <button>Regisztrálok</button>
    </form>
</main>
</body>
</html>